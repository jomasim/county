<?php
 require_once "../member.php";  
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>County</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }

    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">CA</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Projects</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>

</div>

<div class="container text-center">
    <div class="row">
        <div class="col-sm-4">
            <h2>Member Registration</h2>
            <form action="/index.php", method="POST">
                <div class="form-group">
                    <input name="fname", type="text" class="form-control", placeholder="First Name" required >
                </div>
                <div class="form-group">
                    <input name="sname", type="text" class="form-control", placeholder="Second Name" required>
                </div>
                <div class="form-group">
                    <input name="email", type="email" class="form-control" placeholder="Email Address" required>
                </div>
                <div class="form-group">
                    <input name="phone", type="phone" class="form-control" placeholder="Phone" required>
                </div>
                <div class="form-group">
                    <input name="id_number", type="text" class="form-control" placeholder="National ID" required>
                </div>
                <div class="form-group">
                    <input name="address", type="text" class="form-control" placeholder="Postal Address" required>
                </div>
                <div class="form-group">
                    <input name="password", type="password" class="form-control" placeholder="password" required>
                </div>
                <div class="form-group">
                    <input name="_password", type="password" class="form-control" placeholder="confirm password">
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control" name="register", value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>
<br>

<footer class="container-fluid text-center">
    <p>We serve with passion and care</p>
</footer>

</body>
</html>
